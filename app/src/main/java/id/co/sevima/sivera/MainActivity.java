package id.co.sevima.sivera;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import id.co.sevima.sivera.Modul_Dosen.DosenActivity;
import id.co.sevima.sivera.Modul_Mahasiswa.HasilPencarianMahasiswaActivity;
import id.co.sevima.sivera.Modul_Mahasiswa.MahasiswaActivity;


public class MainActivity extends AppCompatActivity {

    Button btnMahasiswa, btnDosen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnDosen = (Button) findViewById(R.id.btnMenuCariDosen);
        btnMahasiswa = (Button) findViewById(R.id.btnMenuCariMahasiswa);
        btnDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, DosenActivity.class);
                startActivity(i);
            }
        });
        btnMahasiswa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, MahasiswaActivity.class);
                startActivity(i);
            }
        });
    }

}