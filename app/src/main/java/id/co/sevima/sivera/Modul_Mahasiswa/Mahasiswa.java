package id.co.sevima.sivera.Modul_Mahasiswa;

/**
 * Created by Asus PC on 04/07/2017.
 */

public class Mahasiswa {
    public String no, nim, nama, jenjang, perguruanTinggi, programStudi, urlDetail;

    public Mahasiswa() {
    }

    public Mahasiswa(String no, String nim, String nama, String jenjang, String perguruanTinggi, String programStudi, String urlDetail) {
        this.no = no;
        this.nim = nim;
        this.nama = nama;
        this.jenjang = jenjang;
        this.perguruanTinggi = perguruanTinggi;
        this.programStudi = programStudi;
        this.urlDetail = urlDetail;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setJenjang(String jenjang) {
        this.jenjang = jenjang;
    }

    public void setPerguruanTinggi(String perguruanTinggi) {
        this.perguruanTinggi = perguruanTinggi;
    }

    public void setProgramStudi(String programStudi) {
        this.programStudi = programStudi;
    }

    public void setUrlDetail(String urlDetail) {
        this.urlDetail = urlDetail;
    }

    public String getNo() {
        return no;
    }

    public String getNim() {
        return nim;
    }

    public String getNama() {
        return nama;
    }

    public String getJenjang() {
        return jenjang;
    }

    public String getPerguruanTinggi() {
        return perguruanTinggi;
    }

    public String getProgramStudi() {
        return programStudi;
    }

    public String getUrlDetail() {
        return urlDetail;
    }
}

