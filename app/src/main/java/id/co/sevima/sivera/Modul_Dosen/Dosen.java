package id.co.sevima.sivera.Modul_Dosen;

/**
 * Created by Andy Yohanes on 13/07/2017.
 */

public class Dosen {
    public String no, nama, gelar, perguruanTinggi, urlDetail;

    public Dosen() {
    }

    public Dosen(String no, String nama, String gelar, String perguruanTinggi, String urlDetail) {
        this.no = no;
        this.nama = nama;
        this.gelar = gelar;
        this.perguruanTinggi = perguruanTinggi;
        this.urlDetail = urlDetail;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setGelar(String gelar) {
        this.gelar = gelar;
    }

    public void setPerguruanTinggi(String perguruanTinggi) {
        this.perguruanTinggi = perguruanTinggi;
    }

    public void setUrlDetail(String urlDetail) {
        this.urlDetail = urlDetail;
    }

    public String getNo() {
        return no;
    }

    public String getNama() {
        return nama;
    }

    public String getGelar() {
        return gelar;
    }

    public String getPerguruanTinggi() {
        return perguruanTinggi;
    }

    public String getUrlDetail() {
        return urlDetail;
    }
}
