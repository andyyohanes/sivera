package id.co.sevima.sivera.Modul_Mahasiswa;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.text.util.Linkify;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import id.co.sevima.sivera.R;

public class DetailMahasiswaActivity extends AppCompatActivity {

    TextView tvNama, tvJenisKelamin, tvPerguruanTinggi, tvProgramStudi, tvNim, tvSemesterAwal, tvStatusAwal, tvStatusMahasiswaSaatIni, tvStatus, tvIsiStatus;
    LinearLayout llTanggalLulus;
    public String url;
    public doit GetDetailMahasiswa = null;
    public LinearLayout llDisclaimer;
    private TextView tvLinkify;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_mahasiswa);

        getSupportActionBar().setTitle("Detail Mahasiswa");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_keyboard_arrow_left_black_24dp);

        url = getIntent().getStringExtra("urlDetail");

        tvNama = (TextView) findViewById(R.id.tvIsiNamaMahasiswa);
        tvJenisKelamin = (TextView) findViewById(R.id.tvIsiJenisKelamin);
        tvPerguruanTinggi = (TextView) findViewById(R.id.tvIsiPerguruanTinggi);
        tvProgramStudi = (TextView) findViewById(R.id.tvIsiProgramStudi);
        tvNim = (TextView) findViewById(R.id.tvIsiNim);
        tvSemesterAwal = (TextView) findViewById(R.id.tvIsiSemesterAwal);
        tvStatusAwal = (TextView) findViewById(R.id.tvIsiStatusAwalMahasiswa);
        tvStatusMahasiswaSaatIni = (TextView) findViewById(R.id.tvIsiStatusMahasiswaSaatIni);
        llTanggalLulus = (LinearLayout) findViewById(R.id.llStatus);
        tvStatus = (TextView) findViewById(R.id.tvStatus);
        tvIsiStatus = (TextView) findViewById(R.id.tvIsiStatus);
        llDisclaimer = (LinearLayout) findViewById(R.id.llDisclaimerDetail);
        tvLinkify = (TextView) findViewById(R.id.tvUrlForlap);
        Linkify.addLinks(tvLinkify, Linkify.ALL);

        GetDetailMahasiswa = new doit(url);
        GetDetailMahasiswa.execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class doit extends AsyncTask<Void, Void, Void> {

        String detail_nama;
        String detail_jenis_kelamin;
        String detail_universitas;
        String detail_program_studi;
        String detail_nim;
        String detail_semester_awal;
        String detail_status_awal;
        String detail_status_mahasiswa;
        String detail_tanggal_lulus;
        String detail_status;

        RelativeLayout rlProgressBar = (RelativeLayout) findViewById(R.id.rlProgressBar2);
        LinearLayout llProfilMahasiswa = (LinearLayout) findViewById(R.id.llProfilMahasiswa);

        String urlDetail;

        public doit(String url) {
            urlDetail = url;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            rlProgressBar.setVisibility(View.VISIBLE);
            llProfilMahasiswa.setVisibility(View.GONE);

        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Document doc = Jsoup.connect(urlDetail).get();

                for (Element table : doc.select("table.table1")) {
                    Elements row = table.select("tr");
                    Elements tds = row.select("td");

                    detail_nama = tds.get(2).text();
                    detail_jenis_kelamin = tds.get(5).text();
                    detail_universitas = tds.get(9).text();
                    detail_program_studi = tds.get(12).text();
                    detail_nim = tds.get(15).text();
                    detail_semester_awal = tds.get(18).text();
                    detail_status_awal = tds.get(21).text();
                    detail_status_mahasiswa = tds.get(24).text();
                    detail_status = tds.get(25).text();
                    detail_tanggal_lulus = tds.get(27).text();

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            tvNama.setText(detail_nama);
            tvJenisKelamin.setText(detail_jenis_kelamin);
            tvPerguruanTinggi.setText(detail_universitas);
            tvProgramStudi.setText(detail_program_studi);
            tvNim.setText(detail_nim);
            tvSemesterAwal.setText(detail_semester_awal);
            tvStatusAwal.setText(detail_status_awal);
            tvStatusMahasiswaSaatIni.setText(detail_status_mahasiswa);
            if (detail_tanggal_lulus != null) {
                tvStatus.setText(detail_status);
                tvIsiStatus.setText(detail_tanggal_lulus);
                llTanggalLulus.setVisibility(View.VISIBLE);
            }
            rlProgressBar.setVisibility(View.GONE);
            llProfilMahasiswa.setVisibility(View.VISIBLE);
            llDisclaimer.setVisibility(View.VISIBLE);

            GetDetailMahasiswa = null;
        }


    }
}
