package id.co.sevima.sivera.Modul_Dosen;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.util.Linkify;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import id.co.sevima.sivera.Modul_Dosen.DetailDosenActivity;
import id.co.sevima.sivera.R;

public class DetailDosenActivity extends AppCompatActivity {

    TextView tvNama, tvJenisKelamin, tvPerguruanTinggi, tvProgramStudi, tvJabatanFungsional, tvPendidikanTertinggi, tvStatusIkatanKerja, tvStatusAktivitas, tvStatus, tvIsiStatus;
    LinearLayout llTanggalLulus;
    public String url;
    public doit GetDetailDosen = null;
    public LinearLayout llDisclaimer;
    private TextView tvLinkify;
    ImageView ivFotoDosen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_dosen);

        getSupportActionBar().setTitle("Detail Dosen");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_keyboard_arrow_left_black_24dp);

        url = getIntent().getStringExtra("urlDetail");

        tvNama = (TextView) findViewById(R.id.tvIsiNamaDosen);
        tvJenisKelamin = (TextView) findViewById(R.id.tvIsiJenisKelaminDosen);
        tvPerguruanTinggi = (TextView) findViewById(R.id.tvIsiPerguruanTinggiDosen);
        tvProgramStudi = (TextView) findViewById(R.id.tvIsiProgramStudiDosen);
        tvJabatanFungsional = (TextView) findViewById(R.id.tvIsiJabatanFungsionalDosen);
        tvPendidikanTertinggi = (TextView) findViewById(R.id.tvIsiPendidikanTertinggiDosen);
        tvStatusIkatanKerja = (TextView) findViewById(R.id.tvIsiStatusIkatanKerja);
        tvStatusAktivitas = (TextView) findViewById(R.id.tvIsiStatusAktivitasDosen);
        llTanggalLulus = (LinearLayout) findViewById(R.id.llStatusDosen);
        tvStatus = (TextView) findViewById(R.id.tvStatusDosen);
        tvIsiStatus = (TextView) findViewById(R.id.tvIsiStatusDosen);
        llDisclaimer = (LinearLayout) findViewById(R.id.llDisclaimerDetailDosen);
        tvLinkify = (TextView) findViewById(R.id.tvUrlForlapDosen);
        Linkify.addLinks(tvLinkify, Linkify.ALL);
        ivFotoDosen = (ImageView) findViewById(R.id.ivFotoDosen);

        GetDetailDosen = new DetailDosenActivity.doit(url);
        GetDetailDosen.execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class doit extends AsyncTask<Void, Void, Void> {

        String detail_nama;
        String detail_jenis_kelamin;
        String detail_universitas;
        String detail_program_studi;
        String detail_jabatan_fungsional;
        String detail_pendidikan_tertinggi;
        String detail_status_ikatan_kerja;
        String detail_status_aktivitas;
        String detail_tanggal_lulus;
        String detail_status;

        RelativeLayout rlProgressBar = (RelativeLayout) findViewById(R.id.rlProgressBar2Dosen);
        LinearLayout llProfilDosen = (LinearLayout) findViewById(R.id.llProfilDosen);

        String urlDetail;
        String urlPhoto;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            rlProgressBar.setVisibility(View.VISIBLE);
            llProfilDosen.setVisibility(View.GONE);
        }

        public doit(String url) {
            urlDetail = url;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Document doc = Jsoup.connect(urlDetail).get();

                Elements picture = doc.select("img.img-polaroid");
                this.urlPhoto = picture.attr("src");
                Log.e("url photo",urlPhoto);

//                Picasso.with(getBaseContext()).load(urlPhoto).into(ivFotoDosen);

                for (Element table : doc.select("table.table1")) {
                    Elements row = table.select("tr");
                    Elements tds = row.select("td");

                    detail_nama = tds.get(2).text();
                    detail_universitas = tds.get(5).text();
                    detail_program_studi = tds.get(8).text();
                    detail_jenis_kelamin = tds.get(11).text();
                    detail_jabatan_fungsional = tds.get(14).text();
                    detail_pendidikan_tertinggi = tds.get(17).text();
                    detail_status_ikatan_kerja = tds.get(20).text();
                    detail_status_aktivitas = tds.get(23).text();
                    detail_status = tds.get(24).text();
                    detail_tanggal_lulus = tds.get(26).text();

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            Picasso.with(getBaseContext()).load(urlPhoto).pl.resize(275,300).into(ivFotoDosen);

            tvNama.setText(detail_nama);
            tvJenisKelamin.setText(detail_jenis_kelamin);
            tvPerguruanTinggi.setText(detail_universitas);
            tvProgramStudi.setText(detail_program_studi);
            tvJabatanFungsional.setText(detail_jabatan_fungsional);
            tvPendidikanTertinggi.setText(detail_pendidikan_tertinggi);
            tvStatusIkatanKerja.setText(detail_status_ikatan_kerja);
            tvStatusAktivitas.setText(detail_status_aktivitas);
            if (detail_tanggal_lulus != null) {
                tvStatus.setText(detail_status);
                tvIsiStatus.setText(detail_tanggal_lulus);
                llTanggalLulus.setVisibility(View.VISIBLE);
            }
            rlProgressBar.setVisibility(View.GONE);
            llProfilDosen.setVisibility(View.VISIBLE);
            llDisclaimer.setVisibility(View.VISIBLE);

            GetDetailDosen = null;

        }


    }
}