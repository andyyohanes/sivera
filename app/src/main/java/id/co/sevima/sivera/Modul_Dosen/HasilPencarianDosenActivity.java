package id.co.sevima.sivera.Modul_Dosen;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

import id.co.sevima.sivera.Modul_Dosen.DetailDosenActivity;
import id.co.sevima.sivera.Modul_Dosen.HasilPencarianDosenActivity;
import id.co.sevima.sivera.Modul_Dosen.Dosen;
import id.co.sevima.sivera.R;
import id.co.sevima.sivera.RecyclerTouchListener;
import id.co.sevima.sivera.adapter.DosenAdapter;

public class HasilPencarianDosenActivity extends AppCompatActivity {

    private ArrayList<Dosen> dosenArrayList = new ArrayList<>();
    private RecyclerView recyclerView;
    private DosenAdapter mAdapter;
    public Dosen dosen;
    public String key;
    public String id_pt;
    public doit GetHasilPencarian = null;
    public TextView tvCariData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasil_pencarian_dosen);

        key = getIntent().getStringExtra("keyword");
        id_pt = getIntent().getStringExtra("id_pt");

        GetHasilPencarian = new doit(key, id_pt);
        GetHasilPencarian.execute();

        getSupportActionBar().setTitle("Mencari Dosen");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_keyboard_arrow_left_black_24dp);

        tvCariData = (TextView) findViewById(R.id.tvCariDataDosen);
        tvCariData.setText("Sedang Proses Mencari "+ key);

        mAdapter = new DosenAdapter(dosenArrayList, this);

        recyclerView = (RecyclerView) findViewById(R.id.lvHasilDosen);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, OrientationHelper.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setRecyclelistview() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, OrientationHelper.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Dosen dosen = dosenArrayList.get(position);
                Intent i = new Intent(HasilPencarianDosenActivity.this, DetailDosenActivity.class);
                i.putExtra("urlDetail", dosen.getUrlDetail());
                //Log.e("link",dosen.getUrlDetail());
                startActivity(i);
            }

            @Override
            public void onLongClick(View view, int position) {
                Dosen dosen = dosenArrayList.get(position);
                Toast.makeText(getApplicationContext(), dosen.getNama() + " long pressed is selected!", Toast.LENGTH_SHORT).show();

            }
        }));

    }

    public class doit extends AsyncTask<Void, Void, Void> {

        String keyword;
        String id_sp;
        LinearLayout rlProgressBar = (LinearLayout) findViewById(R.id.rlProgressBarDosen);
        LinearLayout llNotFound = (LinearLayout) findViewById(R.id.llNotFoundDosen);
        Document doc;


        public doit(String nama, String id_universitas) {
            keyword = nama;
            id_sp = id_universitas;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            rlProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                if(id_sp==null)
                    doc = Jsoup.connect("https://forlap.ristekdikti.go.id/dosen/search").data("keyword", keyword).timeout(120000).userAgent("Mozilla").post();
                else
                    doc = Jsoup.connect("https://forlap.ristekdikti.go.id/dosen/search").data("keyword", keyword).data("id_sp", id_sp).timeout(120000).userAgent("Mozilla").post();

                for (Element tr : doc.select("tr.tmiddle")) {
                    Elements tds = tr.select("td");

                    Elements links = tds.select("a");
                    String link = links.attr("href");

                    dosen = new Dosen(tds.get(0).text(), tds.get(1).text(), tds.get(2).text(), tds.get(3).text(), link);
                    dosenArrayList.add(dosen);

                }


            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (dosenArrayList.size() == 0) {
                llNotFound.setVisibility(View.VISIBLE);
            } else
                setRecyclelistview();

            rlProgressBar.setVisibility(View.GONE);

            getSupportActionBar().setTitle("Hasil Pencarian");

            GetHasilPencarian = null;
        }


    }
}