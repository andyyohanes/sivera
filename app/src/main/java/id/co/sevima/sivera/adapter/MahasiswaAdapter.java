package id.co.sevima.sivera.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import id.co.sevima.sivera.Modul_Mahasiswa.Mahasiswa;
import id.co.sevima.sivera.R;

import java.util.ArrayList;

/**
 * Created by Sentanu on 04/07/2017.
 */

public class MahasiswaAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Mahasiswa> listMahasiswa;
    Context mContext;


    //    1. buat view holder ini dan isikan sesuai layout mahasiswa yang kita buat
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView no, nim, nama, jenjang, perguruanTinggi, programStudi;

        public MyViewHolder(View view) {
            super(view);
            no = (TextView) view.findViewById(R.id.txt_no_mhs);
            nim = (TextView) view.findViewById(R.id.txt_nim_mhs);
            nama = (TextView) view.findViewById(R.id.txt_nama_mhs);
            jenjang = (TextView) view.findViewById(R.id.txt_jenjang_mhs);
            perguruanTinggi = (TextView) view.findViewById(R.id.txt_perguruan_tinggi_mhs);
            programStudi = (TextView) view.findViewById(R.id.txt_program_studi_mhs);
        }
    }


    //pemanggilan class di awal langsung menghandle data arrat sesuai model movies
    public MahasiswaAdapter(ArrayList<Mahasiswa> listMahasiswa, Context context) {
        this.listMahasiswa = listMahasiswa;
        this.mContext = context;
    }

    //    fungsi bawaan extend
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

//        inflate dengan layout yang kita buat agar layput tersebut dapat di tampilkan di recyclerview
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_list_mahasiswa, parent, false);
        return new MyViewHolder(view);
    }

    //    fungsi bawaan extend
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Mahasiswa object = listMahasiswa.get(position);

        if (object != null) {

            ((MyViewHolder) holder).no.setText(object.getNo());
            ((MyViewHolder) holder).nim.setText(object.getNim());
            ((MyViewHolder) holder).nama.setText(object.getNama());
            ((MyViewHolder) holder).jenjang.setText(object.getJenjang());
            ((MyViewHolder) holder).perguruanTinggi.setText(object.getPerguruanTinggi());
            ((MyViewHolder) holder).programStudi.setText(object.programStudi);
            //((MyViewHolder) holder).programStudi.setText(object.programStudi);

        }
    }

    //    fungsi bawaan extend
    @Override
    public int getItemCount() {
//        return 0;
        return listMahasiswa.size();
    }
}
