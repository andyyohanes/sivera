package id.co.sevima.sivera.Modul_Mahasiswa;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import id.co.sevima.sivera.R;
import id.co.sevima.sivera.RecyclerTouchListener;
import id.co.sevima.sivera.adapter.MahasiswaAdapter;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

public class HasilPencarianMahasiswaActivity extends AppCompatActivity {

    private ArrayList<Mahasiswa> mahasiswaArrayList = new ArrayList<>();
    private RecyclerView recyclerView;
    private MahasiswaAdapter mAdapter;
    public Mahasiswa mahasiswa;
    public String key;
    public String id_pt;
    public doit GetHasilPencarian = null;
    public TextView tvCariData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasil_pencarian);

        key = getIntent().getStringExtra("keyword");
        id_pt = getIntent().getStringExtra("id_pt");

        GetHasilPencarian = new doit(key, id_pt);
        GetHasilPencarian.execute();

        getSupportActionBar().setTitle("Mencari Mahasiswa");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_keyboard_arrow_left_black_24dp);

        tvCariData = (TextView) findViewById(R.id.tvCariData);
        tvCariData.setText("Sedang Proses Mencari "+ key);

        mAdapter = new MahasiswaAdapter(mahasiswaArrayList, this);

        recyclerView = (RecyclerView) findViewById(R.id.lvHasil);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, OrientationHelper.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setRecyclelistview() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, OrientationHelper.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Mahasiswa mahasiswa = mahasiswaArrayList.get(position);
                Intent i = new Intent(HasilPencarianMahasiswaActivity.this, DetailMahasiswaActivity.class);
                i.putExtra("urlDetail", mahasiswa.getUrlDetail());
                startActivity(i);
            }

            @Override
            public void onLongClick(View view, int position) {
                Mahasiswa mahasiswa = mahasiswaArrayList.get(position);
                Toast.makeText(getApplicationContext(), mahasiswa.getNim() + " long pressed is selected!", Toast.LENGTH_SHORT).show();

            }
        }));

    }

    public class doit extends AsyncTask<Void, Void, Void> {

        String keyword;
        String id_sp;
        LinearLayout rlProgressBar = (LinearLayout) findViewById(R.id.rlProgressBar);
        LinearLayout llNotFound = (LinearLayout) findViewById(R.id.llNotFound);
        Document doc;


        public doit(String nama, String id_universitas) {
            keyword = nama;
            id_sp = id_universitas;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            rlProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                if(id_sp==null)
                    doc = Jsoup.connect("https://forlap.ristekdikti.go.id/mahasiswa/search").data("keyword", keyword).timeout(120000).userAgent("Mozilla").post();
                else
                    doc = Jsoup.connect("https://forlap.ristekdikti.go.id/mahasiswa/search").data("keyword", keyword).data("id_sp", id_sp).timeout(120000).userAgent("Mozilla").post();

                for (Element tr : doc.select("tr.tmiddle")) {
                    Elements tds = tr.select("td");

                    Elements links = tds.select("a");
                    String link = links.attr("href");

                    mahasiswa = new Mahasiswa(tds.get(0).text(), tds.get(1).text(), tds.get(2).text(), tds.get(3).text(), tds.get(4).text(), tds.get(5).text(), link);
                    mahasiswaArrayList.add(mahasiswa);

                }


            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (mahasiswaArrayList.size() == 0) {
                llNotFound.setVisibility(View.VISIBLE);
            } else
                setRecyclelistview();

            rlProgressBar.setVisibility(View.GONE);

            getSupportActionBar().setTitle("Hasil Pencarian");

            GetHasilPencarian = null;
        }


    }
}
