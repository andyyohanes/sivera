package id.co.sevima.sivera.Modul_Dosen;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import id.co.sevima.sivera.R;

public class DosenActivity extends AppCompatActivity {

    Button btnCari;
    EditText edKeyword;
    AutoCompleteTextView acPerguruanTinggi;
    List<String> valuePT = new ArrayList<>();
    List<String> labelPT = new ArrayList<>();
    ArrayAdapter<String> adapterPT;
    String PerguruanTinggi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen);
        acPerguruanTinggi = (AutoCompleteTextView) findViewById(R.id.acPerguruanTinggiDosen);
        edKeyword = (EditText) findViewById(R.id.edKataKunciDosen);
        btnCari = (Button) findViewById(R.id.btnCariDosen);
        adapterPT = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, labelPT);
        acPerguruanTinggi.setAdapter(adapterPT);
        acPerguruanTinggi.addTextChangedListener(new TextWatcher() {
            private Timer timer = new Timer();
            private final long delay = 500;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(final Editable editable) {
                if(editable.toString().isEmpty())
                    PerguruanTinggi = null;
                if (!acPerguruanTinggi.isPerformingCompletion()) {
                    timer.cancel();
                    timer = new Timer();
                    timer.schedule(
                            new TimerTask() {
                                @Override
                                public void run() {
                                    getPerguruanTinggi(editable.toString());
                                }
                            }, delay
                    );
                }
            }
        });
        acPerguruanTinggi.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                PerguruanTinggi = "";
                PerguruanTinggi = valuePT.get(i);
            }
        });
        btnCari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cekCari();
            }
        });
    }

    private void setAutoComplete() {
        adapterPT = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, labelPT);
        acPerguruanTinggi.setAdapter(adapterPT);
        acPerguruanTinggi.showDropDown();
    }

    private void cekCari() {
        if (edKeyword.getText().toString().equals("")) {
            edKeyword.setError("Harap diisi");
        } else {
            Intent i = new Intent(DosenActivity.this, HasilPencarianDosenActivity.class);
            i.putExtra("keyword", edKeyword.getText().toString());
            i.putExtra("id_pt", PerguruanTinggi);
            startActivity(i);
        }
    }

    private void getPerguruanTinggi(String nama) {
        nama = nama.replaceAll(" ","%20");
        valuePT.clear();
        labelPT.clear();
        String url = "https://forlap.ristekdikti.go.id/ajax/listPT/" + nama;
        RequestQueue rq = Volley.newRequestQueue(this);
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray items = response.getJSONArray("items");
                            for (int i = 0; i < items.length(); i++) {
                                JSONObject object = items.getJSONObject(i);
                                valuePT.add(object.getString("value"));
                                labelPT.add(object.getString("label"));
                            }
                            if(!acPerguruanTinggi.getText().toString().isEmpty()){
                                setAutoComplete();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        );
        rq.add(jor);
    }
}
