package id.co.sevima.sivera.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.sevima.sivera.Modul_Dosen.Dosen;
import id.co.sevima.sivera.R;

/**
 * Created by Andy Yohanes on 13/07/2017.
 */

public class DosenAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Dosen> listDosen;
    Context mContext;


    //    1. buat view holder ini dan isikan sesuai layout dosen yang kita buat
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView no, nip, nama, gelar, perguruanTinggi;

        public MyViewHolder(View view) {
            super(view);
            no = (TextView) view.findViewById(R.id.txt_no_dosen);
            nip = (TextView) view.findViewById(R.id.txt_nip_dosen);
            nama = (TextView) view.findViewById(R.id.txt_nama_dosen);
            gelar = (TextView) view.findViewById(R.id.txt_gelar_dosen);
            perguruanTinggi = (TextView) view.findViewById(R.id.txt_perguruan_tinggi_dosen);
        }
    }


    //pemanggilan class di awal langsung menghandle data arrat sesuai model movies
    public DosenAdapter(ArrayList<Dosen> listDosen, Context context) {
        this.listDosen = listDosen;
        this.mContext = context;
    }

    //    fungsi bawaan extend
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

//        inflate dengan layout yang kita buat agar layput tersebut dapat di tampilkan di recyclerview
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_list_dosen, parent, false);
        return new MyViewHolder(view);
    }

    //    fungsi bawaan extend
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Dosen object = listDosen.get(position);

        if (object != null) {

            ((MyViewHolder) holder).no.setText(object.getNo());
            ((MyViewHolder) holder).nama.setText(object.getNama());
            ((MyViewHolder) holder).gelar.setText(object.getGelar());
            ((MyViewHolder) holder).perguruanTinggi.setText(object.getPerguruanTinggi());

        }
    }

    //    fungsi bawaan extend
    @Override
    public int getItemCount() {
//        return 0;
        return listDosen.size();
    }
}
